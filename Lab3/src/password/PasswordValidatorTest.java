package password;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMain() {
		fail("Not yet implemented");
	}
	
	@Test //Good Condition
	public void testCheckPasswordLength() {
		assertTrue("Invalid password lenght",
				PasswordValidator.checkPasswordLength("thisisavalidpassword"));
	}
		
	@Test //Bad Condition
	public void testCheckPasswordLengthFailed() {
		assertTrue("Password not long enough",
				PasswordValidator.checkPasswordLength("abc"));
	}
	
	@Test //Boundary In Condition
	public void testCheckPasswordLengthBoundayIn() {
		assertTrue("Password not long enought",
				PasswordValidator.checkPasswordLength("abcdefgh"));
	}
	
	@Test //Boundary Out Condition
	public void testCheckPasswordLengthBoundayOut() {
		assertTrue("Password not long enought",
				PasswordValidator.checkPasswordLength("abcdefg"));
	}
	
	@Test
	public void testCheckPasswordDigits() {
		assertTrue("Password not Valid",
				PasswordValidator.checkPasswordDigits("12345678"));
	}
	
	@Test
	public void testCheckPasswordDigitsFailed() {
		assertTrue("Password not Valid",
				PasswordValidator.checkPasswordDigits("abcdefgh"));
	}
	
	@Test
	public void testCheckPasswordDigitsBoundaryIn() {
		assertTrue("Password not Valid",
				PasswordValidator.checkPasswordDigits("abcd1efg3h"));
	}
	
	@Test
	public void testCheckPasswordDigitsBoundaryOut() {
		assertTrue("Password not Valid",
				PasswordValidator.checkPasswordDigits("abc2efgh"));
	}
	
	


	

}
