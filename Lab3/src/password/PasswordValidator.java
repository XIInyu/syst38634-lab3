package password;

public class PasswordValidator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static boolean checkPasswordLength(String password) {
		if(password.length()<8) {
			return false;
		}
		return true;	
	}
	
	public static boolean checkPasswordDigits(String password) {
	
		String pattern = "^(?=(?:\\D*\\d){2})[a-zA-Z0-9]*$";
		return password.matches(pattern);
	}

}
